class 389client{
    include yum,sysctl

	#public ca kopieren
    file { "dirsrv01_public_CA.asc":
        path => "/etc/openldap/cacerts/dirsrv01_public_CA.asc",
        source => "puppet:///modules/389client/dirsrv01_public_CA.asc",
        mode => 0644,
        owner => root,
        group => root,
        require => [
          Package["nss-pam-ldapd"],
        ],
    }

    package { 'nss-pam-ldapd':
        ensure   => installed,
        provider => 'yum',
        require  => [Package["openldap-clients"]],
    }

    package { 'openldap-clients':
        ensure   => installed,
        provider => 'yum',
    }

	#Host entry fuer dirsrv01
	host { 'dirsrv01.stxt.mpc':
		ip => '10.100.219.85',
		host_aliases => 'dirsrv01',
	}

	#folder fuer cacerts
	file { "/etc/openldap/cacerts/":
		ensure => "directory",
	}

	#rehash des certs
    exec{ "/usr/sbin/cacertdir_rehash /etc/openldap/cacerts/":
        cwd => "/",
        require =>[File["dirsrv01_public_CA.asc"]],
    }

	#Konfiguration der Authentifizierung
    exec{ "/usr/sbin/authconfig --disableshadow --enablemd5 --enableldap --enableldaptls --ldapserver=ldap://dirsrv01.stxt.mpc:389 --ldapbasedn='ou=${customerOU},dc=stxt,dc=mpc' --enableldapauth --update":
        cwd => "/",
        require => [exec["/usr/sbin/cacertdir_rehash /etc/openldap/cacerts/"]],
        notify => Service["nslcd"], #service restart
    }
	
	#Service restart
	service{"nslcd":
		ensure => "running",
		enable => "true",
	}
}
